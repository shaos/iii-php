/* Check correctness of ii/IDEC file DB - written by Shaos in Oct 2024 and moved to PUBLIC DOMAIN */

/* Usage - run it in echo directory (msg must be available as ../msg)

   ./iii-check *.*
   ./iii-check ANY.ECHO

   If only 1 argument used then it will do deeper analysis with creating iii-check.out file
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

int main(int argc, char **argv)
{
 int i,j,k,mi,ma,ok;
 unsigned long t0,t1,t2;
 FILE *f,*f2,*f3=NULL;
 char str[100],st1[256],st2[256],st3[256],*po,*po1;
 t0 = time(NULL)+24L*60L*60L;
 if(argc==2)
 {
   f3 = fopen("iii-check.txt","wt");
   if(f3==NULL) return -1;
 }
 for(i=1;i<argc;i++)
 {
   po = strrchr(argv[i],'.');
   if(po)
   {
     if(strstr(po,".c")) continue;
     if(strstr(po,".txt")) continue;
     if(strstr(po,".html")) continue;
   }
   else continue;
   printf("[%i] %s\n",i,argv[i]);
   f = fopen(argv[i],"rt");
   if(f==NULL)
   {
     printf("ERROR: can't open %s\n",argv[i]);
     continue;
   }
   t1 = j = 0;
   while(1)
   {
     fgets(str,100,f);
     str[99] = 0;
     if(feof(f)) break;
     po = strrchr(str,'\n');
     if(po!=NULL) *po = 0;
     po = strrchr(str,'\r');
     if(po!=NULL) *po = 0;
     mi = 256;
     ma = 0;
     for(k=0;k<strlen(str);k++)
     {
        if(str[k] < mi) mi=str[k];
        if(str[k] > ma) ma=str[k];
     }
     j++;
     if(k!=20||mi<'0'||ma>'z')
     {
        printf("ERROR: wrong msgid %s [%i]\n",str,j);
        continue;
     }
     strcpy(st2,argv[1]);
     po = strrchr(st2,'/');
     if(po!=NULL){po++;*po=0;}
     else *st2=0;
     strcat(st2,"../msg/");
     strcat(st2,str);
//     printf("%s\n",st2);
     f2 = fopen(st2,"rt");
     if(f2==NULL) printf("ERROR: can't open %s [%i]\n",st2,j);
     else
     {
        ok = 0;
        fgets(st1,256,f2);
        if(!feof(f2))
        {
          st1[255]=0;
          po = strstr(st1,"/reply/");
          if(po!=NULL)
          {
             po1 = &po[7];
             po = strchr(po1,'\n');
             if(po!=NULL) *po=0;
             po = strchr(po1,'\r');
             if(po!=NULL) *po=0;
             po = strchr(po1,'/');
             if(po!=NULL) *po=0;
             printf(">>>%s<<<\n",po1);
          }
          fgets(st2,256,f2);
          if(!feof(f2))
          {
            st2[255]=0;
            po = strchr(st2,'\n');
            if(po!=NULL) *po=0;
            po = strchr(st2,'\r');
            if(po!=NULL) *po=0;
            po = strrchr(argv[i],'/');
            if(po!=NULL) po++;
            else po = argv[i];
            if(strncmp(po,st2,strlen(st2))) printf("ERROR: unexpected echo %s (must be %s) in %s [%i]\n",st2,po,str,j);
            fgets(st3,256,f2);
            if(!feof(f2))
            {
              t2 = atoi(st3);
              if(t2<=1388563200 || t2>t0) printf("ERROR: wrong time %lu in %s [%i]\n",t2,str,j);
              else if(t2<t1) printf("ERROR: past time %lu (-%lu) in %s [%i]\n",t2,t1-t2,str,j);
              t1 = t2;
              ok = 1;
              if(f3) fprintf(f3,"%lu %s\n",t1,str);
            }
          }
        }
        if(!ok) printf("ERROR: invalid message %s\n",str);
        fclose(f2);
     }
//     if(j>10) break;
   }
   fclose(f);
 }
 if(f3) fclose(f3);
 return 0;
}

