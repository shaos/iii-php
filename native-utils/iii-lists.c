/* ii/IDEC lists analyzer - written by Shaos in Oct 2024 and moved to PUBLIC DOMAIN */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define STRLN 1024
#define ECHOS 256
#define ECHON 128
#define LISTN 10

struct _echo
{
  char name[ECHON];
  long result;
  long count[LISTN*2];
} echos[ECHOS];

char str[STRLN];

int main(int argc, char **argv)
{
 int i,j,k,old;
 long l,c0,c1,c2;
 char *po;
 FILE *f;
 if(argc<2 || !(argc&1))
 {
    printf("\nUsage:\n\t./iii-lists LIST.NEW LIST.OLD [LIST2.NEW LIST2.OLD ...]\n\n");
    return -1;
 }
 for(i=0;i<ECHOS;i++)
 {
    echos[i].name[0] = 0;
    echos[i].result = 0;
    for(j=0;j<LISTN;j++)
    {
       echos[i].count[j+j] = -1;
       echos[i].count[j+j+1] = -1;
    }
 }
 old = 0;
 for(k=1;k<argc;k++)
 {
    f = fopen(argv[k],"rt");
    while(1)
    {
       fgets(str,STRLN,f);
       if(feof(f)) break;
       str[STRLN-1] = 0;
       po = strrchr(str,'\n');
       if(po!=NULL) *po=0;
       po = strrchr(str,'\r');
       if(po!=NULL) *po=0;
       po = strtok(str,":");
       if(po==NULL) continue;
       for(i=0;i<ECHOS;i++)
       {
          if(!strcmp(echos[i].name,po) || echos[i].name[0]==0) break;
       }
       if(i==ECHOS) break;
       if(echos[i].name[0]==0) strcpy(echos[i].name,po);
       po = strtok(NULL,":");
       if(po==NULL) continue;
       l = strtol(po,NULL,10);
       for(j=0;j<LISTN;j++)
       {
          if(echos[i].count[j]<0)
          {
             echos[i].count[j] = l;
             break;
          }
       }
       if(old && !(j&1))
       {
          echos[i].count[j+1] = l;
#if 1
          printf("Corrected %s from %s (%lu -> %lu)\n",echos[i].name,argv[k],l,l);
#endif
       }
    }
    fclose(f);
    if(old)
         old = 0;
    else old = 1;
 }
 for(i=0;i<ECHOS;i++)
 {
    if(echos[i].name[0]==0) break;
    printf("[%i] %s",i,echos[i].name);
    for(j=0;j<LISTN;j++)
    {
        c2 = echos[i].count[j+j];
        c1 = echos[i].count[j+j+1];
        if(c2<0) break;
        if(c1<0) c1 = 0;
        c0 = c2 - c1;
        if(c0<0) c0 = 0;
        printf(" | %li -> %li (%li)",c1,c2,c0);
        if(c0 > echos[i].result) echos[i].result = c0;
    }
    if(echos[i].result==0) printf(" |\n");
    else
    {
        printf(" | >>>>> %li\n",echos[i].result);
        for(j=strlen(echos[i].name);j<24;j++) echos[i].name[j]='.';
        if(echos[i].result<=9) sprintf(&echos[i].name[22],".%li",echos[i].result);
        else if(echos[i].result<=99) sprintf(&echos[i].name[21],".%li",echos[i].result);
        else if(echos[i].result<=999) sprintf(&echos[i].name[20],".%li",echos[i].result);
        else if(echos[i].result<=9999) sprintf(&echos[i].name[19],".%li",echos[i].result);
    }
 }
/*
====
Echoareas
────────────────────────
bot.slashdot.........134 ██████████████████████████████████████████████████▒▒
idec.talks............69 ██████████████████████████████████████████████████▒
lor.opennet...........44 ████████████████████████████████████████████
bot.habr.rss..........31 ███████████████████████████████
idec.test..............8 ████████
spnet.stats............5 █████
linux.14...............2 ██
────────────────────────
Total                293

          11111111112222
012345678901234567890123
*/
 l = 0;
 printf("====\n");
 printf("Echoareas\n");
 printf("────────────────────────\n");
 while(1)
 {
    c0 = k = 0;
    for(i=0;i<ECHOS;i++)
    {
       if(echos[i].result > c0)
       {
          j = i;
          c0 = echos[i].result;
          k++;
       }
    }
    if(!k) break;
    echos[j].result = 0;
    l += c0;
    printf("%s ",echos[j].name);
    c1 = 0;
    if(c0>50)
    {
       c1 = 0;
       while(c0>50)
       {
         c1++;
         c0-=50;
       }
       c0 = 50;
    }
    for(i=0;i<c0;i++) printf("█");
    for(i=0;i<c1;i++) printf("▒");
    printf("\n");
 }
 printf("────────────────────────\n");
 printf("Total           %8li\n",l);
 printf("====\n");
 return 0;
}
