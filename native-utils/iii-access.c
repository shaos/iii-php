/* Apache2 logs analysis for iii-php - written by Shaos in Oct 2024 and moved to PUBLIC DOMAIN */

/* Use it daily after log rotation):

   ./iii-access /var/log/apache2/access.log.1

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define STRMAXLEN 10000

char str[STRMAXLEN];

typedef struct ipinfo_
{
  char ip[16];
  unsigned long size;
  unsigned int events[2];
  char last[20];
  void* next;
} ipinfo;

ipinfo* first = NULL;

ipinfo* findipinfo(char *fip, char *rest)
{
  char sip[16],*po;
  strncpy(sip,fip,16);
  sip[15] = 0;
  po = strchr(sip,'.');
  if(po==NULL) exit(-101);
  po = strchr(&po[1],'.');
  if(po==NULL) exit(-102);
  po = strchr(&po[1],'.');
  if(po==NULL) exit(-103);
  po[1] = 'x';
  po[2] = 0;
#if 1
  if(strstr(rest,"Googlebot")) strcpy(sip,"Google");
  else if(strstr(rest,"GoogleOther")) strcpy(sip,"Google");
  else if(strstr(rest,"YandexBot")) strcpy(sip,"Yandex");
  else if(strstr(rest,"Amazonbot")) strcpy(sip,"Amazon");
  else if(strstr(rest,"PentalBot")) strcpy(sip,"PetalBot");
  else if(strstr(rest,"Synapse")) strcpy(sip,"Synapse");
  else if(strstr(rest,"DataForSeoBot")) strcpy(sip,"DataForSeoBot");
  else if(strstr(rest,"facebook.com")) strcpy(sip,"Facebook");
  else if(strstr(rest,"panscient.com")) strcpy(sip,"Panscient");
  else if(strstr(rest,"bytedance.com")) strcpy(sip,"TikTok");
  else if(strstr(rest,"GPTBot")) strcpy(sip,"ChatGPT");
#endif
  ipinfo *oip = NULL;
  ipinfo *ip = first;
  while(ip)
  {
     if(!strcmp(sip,ip->ip)) return ip;
     oip = ip;
     ip = ip->next;
  }
  ip = malloc(sizeof(ipinfo));
  if(!ip) exit(-100);
  if(oip==NULL) first = ip;
  else oip->next = ip;
  strncpy(ip->ip,sip,16);
  ip->ip[15] = 0;
  ip->next = NULL;
  ip->last[0] = 0;
  ip->events[0] = 0;
  ip->events[1] = 0;
  ip->size = 0;
  return ip;
}

int main(int argc, char** argv)
{
 FILE *f,*fo;
 unsigned long m,total=0;
 int i=0,j,k,e1,e2,p,e1hr,name1st;
 char *po,*poo,s[100];
 char *p_ip;
 double d;
 ipinfo *ip, *ip1;
 if(argc<2) return -1;
 f = fopen(argv[1],"rt");
 if(f==NULL) return -2;
 while(1)
 {
   fgets(str,STRMAXLEN,f);
   if(feof(f)) break;
   str[STRMAXLEN-1] = 0;
   if(*str==':') continue;
   if(!strstr(str,"ii")) continue;
   po = strrchr(str,'\n');
   if(po) *po = 0;
   p_ip = str;
   po = strchr(str,' ');
   if(po==NULL) break;
   *po++ = 0;
   po = strchr(po,'[');
   if(po==NULL) break;
   po++;
   strncpy(s,po,17);
   s[17] = 0;
   ip = findipinfo(p_ip,po);
   if(ip==NULL) break;
   if(strcmp(ip->last,s))
   {
      strcpy(ip->last,s);
      if(strstr(po,"/iii/")) ip->events[0]++;
      else if(strstr(po,"ii-point")) ip->events[0]++;
      else if(strstr(po,"ii-web")) ip->events[1]++;
   }
   po = strstr(po,"\" 200 ");
   if(po)
   {
     po = &po[6];
     j = atoi(po);
     ip->size += j;
     total += j;
   }
   else j = 0;
   i++;
#if 0
   printf("|%s|%s|%i|%i|%i|%i|\n",p_ip,s,ip->events[0],ip->events[1],j,ip->size);
   if(i>=100) break;
#endif
 }
 fclose(f);
// printf("Processed %i lines\n\n",i);
 printf("TOP10 VISITORS:\n\n");
 fo = fopen("status","wt");
 if(fo==NULL) return -3;
 name1st = 1;
 fprintf(fo,"Last day top uplinks:");
 for(i=1;i<=10;i++)
 {
    m = 0;
    j = 0;
    e1 = e2 = k = -1;
    ip1 = NULL;
    ip = first;
    while(ip)
    {
       if(ip->size > m)
       {
          strcpy(s,ip->ip);
          m = ip->size;
          e1 = ip->events[0];
          e2 = ip->events[1];
          ip1 = ip;
          k = j;
       }
       ip = ip->next;
       j++;
    }
    if(k<0) break;
    d = m/1024.0/1024.0;
    printf("[%i] %s point=%i web=%i ",i,s,e1,e2);
    if(d>=0.1)
    {
       printf("up=%2.1fMB ",d);
       p = (int)(m*100.0/total);
       if(p>0) printf("(%i%%)",p);
       else printf("(<1%%)");
    }
    else printf("up=%iKB",m>>10);
    e1hr = 0;
    if(e1)
    {
       if(e1>=12)
       {
          e1hr = e1/24;
          if(e1/24.0-e1hr>=0.5) e1hr++;
       }
       printf(" <--- ");
#if 1
       if(!strcmp(s,"80.87.199.x")) strcpy(s,"tgi");
       else if(!strcmp(s,"62.109.31.x")) strcpy(s,"tavern");
       else if(!strcmp(s,"176.109.111.x")) strcpy(s,"tavern");
       else if(!strcmp(s,"95.165.9.x")) strcpy(s,"ping");
       else if(!strcmp(s,"217.197.116.x")) strcpy(s,"blackcat");
       else if(!strcmp(s,"24.130.121.x")) strcpy(s,"spnet");
       else if(!strcmp(s,"217.114.158.x")) strcpy(s,"fox");
       else if(!strcmp(s,"37.252.14.x")) strcpy(s,"ake");
       printf("%s ",s);
#endif
       if(e1hr) printf("(%i/hr)",e1hr);
    }
    printf("\n");
    ip1->size = 0;
    if(e1>5 && d>=0.1)
    {
       if(name1st) name1st=0;
       else fputc(',',fo);
       fprintf(fo," %s %2.1fMB",s,d);
       if(e1hr) fprintf(fo," (%i/hr)",e1hr);
    }
 }
 printf("\nTOTAL TRAFFIC: %luMB\n\n",total>>20);
 fclose(fo);
 return 0;
}
