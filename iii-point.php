<?php
require("ii-functions.php");
header ('Content-Type: text/plain; charset=utf-8');


function remote ($key, $post=true) {
	$arr=($post) ? $_POST : $_GET;
	return (isset($arr[$key]) && !empty($arr[$key]));
}

if (remote('q', false)) {
	$q = $_GET['q'];
	$opts = [];

	$all_options = explode('/', $q);
	foreach ($all_options as $option) {
		if (!empty($option)) $opts[]=$option;
	}

	$optc = count($opts);
	if ($optc == 0) {
		if(isset($main_path)) {
			header("HTTP/1.1 302 Found");
			header("Location: ".$main_path."iii-web.php");
			exit();
		} else {
			die("error: can't parse GET arguments\n");
		}
	}
} else {
	die("error: please specify API query with 'q' GET parameter\n");
}

if ($opts[0] == 'blacklist.txt') {
	echo implode("\n", $access->blacklist);
}

elseif ($opts[0] == 'node.json') {
	$myfile = fopen("node.json", "r") or die("error: node.json is not available\n");
	echo fread($myfile,filesize("node.json"));
	fclose($myfile);
}

elseif ($opts[0] == 'list.txt') {
	displayEchoList(null, $counter=true, $descriptions=true);
}

elseif ($opts[0] == 'list.txt?h=1' || $opts[0] == 'listhsh.txt') {
	displayEchoList(null, $counter=true, $descriptions=false, $hashes=2);
}

elseif ($optc < 2) die("error: wrong arguments\n");

elseif ($opts[0] == 'e') {
	echo implode("\n", $access->getMsgList($opts[1]))."\n";
}

elseif ($opts[0] == 'm') {
	echo $access->getRawMessage($opts[1]);
}

elseif ($opts[0] == 'u' and $opts[1] == 'm') {
	if ($optc == 2) die("error: where are your msgids?\n");

	$msgids=array_slice($opts, 2);
	$messages=$access->getRawMessages($msgids);

	foreach($messages as $msgid => $text) {
		echo $msgid.":".base64_encode($text)."\n";
	}
}

elseif (($opts[0] == 'u' and $opts[1] == 'e') ||
        ($opts[0] == 'lim' and $opts[2] == 'u' and $opts[3] == 'e')) {
	$work_options=array_slice($opts, 2);
	// lim/N/u/e hack
	if($opts[0] == 'lim') {
		$work_options[0] = 'lim';
		$work_options[1] = $opts[1];
	}
	$aecho = [];
	$aoff = [];
	$alen = [];
	$atim = [];
	$lim = 0;
	$count = 0;
	foreach ($work_options as $work) {
		if(is_numeric($work)) {
			$num = intval($work);
			if($num < 0) die("error: unexpected negative number\n");
			if($num >= 1000000000) {
				if($lim != 0) die("error: timestamp can not be used after lim\n");
				for($i=$count-1;$i>=0;$i--) {
					if(!is_null($aoff[$i])) break;
					if(!is_null($atim[$i])) break;
					$atim[$i] = $num;
				}
			} else {
				if($lim >= 0) die("error: unexpected single number\n");
				$lim = $num;
			}
		} elseif(strpos($work,".")!==false) {
			if($lim < 0) die("error: missing lim value\n");
			array_push($aecho,$work);
			if($lim > 0) {
				array_push($aoff,-$lim);
				array_push($alen,$lim);
			} else {
				array_push($aoff,NULL);
				array_push($alen,NULL);
			}
			array_push($atim,NULL);
			$count = $count + 1;
		} elseif(strpos($work,":")!==false || strcmp($work,"all")==0 || strcmp($work,"last")==0) {
			if($lim != 0) die("error: slice can not be used after lim\n");
			if(strcmp($work,"all")==0) {
				$a = 0;
				$b = 0;
			} elseif(strcmp($work,"last")==0) {
				$a = -1;
				$b = 1;
			} else {
				$numbers=explode(":", $work);
				$a = intval($numbers[0]);
				$b = intval($numbers[1]);
				if($a!=0 && $b==0) {
					$b = 999999999;
				}
			}
			for($i=$count-1;$i>=0;$i--) {
				if(!is_null($aoff[$i])) break;
				if(!is_null($atim[$i])) break;
				$aoff[$i] = $a;
				$alen[$i] = $b;
			}
		} elseif(strcmp($work,"lim")==0) {
			$lim = -1;
		} else {
			if($lim != 0) die("error: hash can not be used after lim\n");
			for($i=$count-1;$i>=0;$i--) {
				if(!is_null($aoff[$i])) break;
				if(!is_null($atim[$i])) break;
				$atim[$i] = $work;
			}
		}
	}
	$buffer = "";
	for($i=0;$i<$count;$i++) {
		$echo = $aecho[$i];
		if($aoff[$i]==0 && $alen[$i]==0 && is_null($atim[$i])) {
			$slice = $access->getMsgList($echo); // NULL, NULL, MULL
		} else {
			$slice = $access->getMsgList($echo, $aoff[$i], $alen[$i], $atim[$i]);
		}
		if (count($slice) > 0) {
			$buffer.=$echo."\n".implode("\n", $slice)."\n";
		} else {
			$buffer.=$echo."\n";
		}
	}
	echo $buffer;
}

elseif ($opts[0] == 'u' and $opts[1] == 'point') {
	if ($optc == 4) {
		$au=$opts[2];
		$ms=$opts[3];
	} elseif (remote('pauth') && remote('tmsg')) {
		$au=$_POST['pauth'];
		$ms=$_POST['tmsg'];
	} else die("error: wrong arguments\n");

	if (strlen($ms) > $postlimit) die("error: msg big!\n");

	if ($point=checkUser($au)) {
		$authname=$point[0];
		$addr=$point[1];

		pointSend($ms, $authname, $nodeName.", ".$addr);
	} else {
		die("error: no auth!\n");
	}
}

elseif ($opts[0] == 'u' and $opts[1] == 'push') {
	if (remote('nauth') && remote('upush') && remote('echoarea')) {
		if (!empty($pushpassword) && $_POST['nauth'] === $pushpassword) {
			$bundle=explode("\n", $_POST['upush']);
			foreach ($bundle as $line) {
				$pieces=explode(":", $line);
				if (count($pieces)==2) {
					$msgid=$pieces[0];
					$message=b64d($pieces[1]);
					$r=$access->saveMessage($msgid, $_POST['echoarea'], $message, $raw=true);
					if ($r) echo "message saved: ok: ".$msgid."\n";
					else echo "error saving message ".$msgid."\n";
				} else {
					echo "error: wrong data; continue...\n";
				}
			}
		} else die("error: no auth\n");
	} else die("error: wrong arguments\n");
}

elseif ($opts[0] == 'x' and $opts[1] == 'c') {
	$echos=array_slice($opts, 2);
	displayEchoList($echos, $counter=true, $descriptions=false);
}

elseif ($opts[0] == 'x' and $opts[1] == 'h') {
	$echos=array_slice($opts, 2);
	displayEchoList($echos, $counter=false, $descriptions=false, $hashes=1);
}

elseif ($opts[0] == 'x' and $opts[1] == 'e' and remote('data')) {
	$lines=explode("\n", $_POST['data']);
	foreach ($lines as $line) {
		$line=explode(":", $line);
		if (count($line)!=2) continue;

		$echoarea=trim($line[0]);
		$msgid=trim($line[1]);

		$index=$access->getMsgList($echoarea);
		$maxElement=count($index)-1;

		$search=array_search($msgid, $index);
		if ($search!=NULL and $search<$maxElement) {
			$newMessages=array_slice($index, $search+1);
			echo $echoarea."\n".implode("\n", $newMessages)."\n";
		} elseif ($search==$maxElement) {
			continue;
		} else {
			echo $echoarea."\n".$msgids;
		}
	}
}

elseif ($opts[0] == 'x' and $opts[1] == 'filelist') {
	if (remote('pauth')) {
		$authstr=$_POST['pauth'];
	} elseif ($optc == 3) {
		$authstr=$opts[2];
	} else $authstr=false;

	$filenames=array_keys($public_files);
	$files_info=$public_files;

	if ($authstr!=false && checkUser($authstr) != false) {
		$private_filenames=array_keys($private_files);
		$filenames=array_merge($filenames, $private_filenames);
		$files_info=array_merge($files_info, $private_files);
	}

	foreach ($filenames as $filename) {
		if (@file_exists($files_directory."/".$filename)) {
			echo $filename.":".filesize($files_directory."/".$filename).":".$files_info[$filename]."\n";
		}
	}
}

elseif ($opts[0] == 'x' and $opts[1] == 'file') {
	$filenames=array_keys($public_files);
	$private_filenames=array_keys($private_files);

	if (remote('pauth'))
		$authstr=$_POST['pauth'];
	elseif ($optc == 4)
		$authstr=$opts[2];
	else $authstr=false;

	if (remote('filename')) {
		$request_file=$_POST['filename'];
	} elseif ($optc == 4) {
		$request_file=$opts[3];
	} elseif ($optc == 3) {
		$request_file=$opts[2];
	} else die("error: specify file name\n");

	if ($authstr!=false && checkUser($authstr) != false) {
		// it means "our" user and allowed to dowinload "hidden" files
		$filenames=array_merge($filenames, $private_filenames);
	}

	$file_path=$files_directory."/".$request_file;

	if (in_array($request_file, $filenames) && @file_exists($file_path)) {
		// output binary file
		header ('Content-Type: application/octet-stream');
		header ('Content-Disposition: attachment; filename="'.$request_file.'"');
		if (ob_get_level()) ob_end_clean();

		@readfile($file_path);
		exit();
	} else echo "error: file does not exist or wrong authstr\n";
}

elseif ($opts[0] == 'x' and $opts[1] == 'features') {
	// list extra features
	echo "u/e\nu/push\nlist.txt\nlist.txt?h=1\nlisthsh.txt\nblacklist.txt\nx/c\nx/h\nx/file\nx/filelist\nnode.json\n";
}

else {
	echo "error: wrong api calls\n";
}

?>
